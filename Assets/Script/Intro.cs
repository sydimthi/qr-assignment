﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Intro : MonoBehaviour
{
	void Awake()
	{
		Screen.autorotateToPortrait = true;
		Screen.autorotateToPortraitUpsideDown = true;
		QualitySettings.vSyncCount = 1;
	}

	IEnumerator Start()
	{
		yield return Application.RequestUserAuthorization(UserAuthorization.WebCam);

		if (!Application.HasUserAuthorization(UserAuthorization.WebCam))
		{
			throw new Exception("This Webcam library can't work without the webcam authorization");
		}
	}

	
	public void OnContinuous()
	{
		SceneManager.LoadScene("Scanner");
	}
	public void view()
	{
		SceneManager.LoadScene("view");
	}

	
}